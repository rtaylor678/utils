﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq; //needed for SequenceEqual

namespace Solomon.Common.CryptographyTest
{
    [TestClass]
    public class Password
    {
        int length = 256;
        byte[] password = new byte[128];

        [TestMethod]
        public void PasswordGeneration()
        {
            byte[] generatedPassword;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Generate(length, out generatedPassword), "Generating password failed.");

            Assert.IsTrue(generatedPassword.Length == length, "Unexpected password length.");
        }

        [TestMethod]
        public void PasswordGenerationRandom()
        {
            byte[] passwordA;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Generate(length, out passwordA), "Generating password failed (passwordA)");

            byte[] passwordB;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Generate(length, out passwordB), "Generating password failed (passwordB)"); ;

            Assert.IsFalse(passwordA.SequenceEqual(passwordB), "Passwords should not match.");
        }

        [TestMethod]
        public void PasswordDerive()
        {
            byte[] salt = System.Text.Encoding.ASCII.GetBytes("01234567");

            byte[] derivedPassword;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Derive(password, salt, length, out derivedPassword), "Deriving password failed.");

            Assert.IsTrue(derivedPassword.Length == length);
        }

        [TestMethod]
        public void PasswordDeriveShortSalt()
        {
            byte[] salt = System.Text.Encoding.ASCII.GetBytes("0123");

            byte[] derivedPassword;
            Assert.IsFalse(Solomon.Common.Cryptography.Password.Derive(password, salt, length, out derivedPassword), "Deriving password did not fail.");

            Assert.IsTrue(derivedPassword.Length == 0);
        }

        [TestMethod]
        public void PasswordVerify()
        {
            byte[] salt = System.Text.Encoding.ASCII.GetBytes("01234567");

            byte[] derivedPassword;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Derive(password, salt, length, out derivedPassword), "Deriving password failed.");

            Assert.IsTrue(Solomon.Common.Cryptography.Password.Verify(password, salt, derivedPassword), "Verifying password failed.");
        }

        [TestMethod]
        public void PasswordVerifyShortSalt()
        {
            byte[] salt = System.Text.Encoding.ASCII.GetBytes("0123");

            byte[] expectedPassword = new byte[140];
            Assert.IsFalse(Solomon.Common.Cryptography.Password.Verify(password, salt, expectedPassword), "Verification should not have passed.");
        }

        [TestMethod]
        public void PasswordCheckSlowEqualsGoodPassword()
        {
            byte[] salt = System.Text.Encoding.ASCII.GetBytes("0123456789");

            byte[] passwordInitial;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Derive(password, salt, length, out passwordInitial), "Deriving password failed (passwordInitial).");

            byte[] passwordEqual = passwordInitial;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.SlowEquals(passwordInitial, passwordEqual), "Slow equals failed to verify password.");
        }

        [TestMethod]
        public void PasswordCheckSlowEqualsBadPassword()
        {
            byte[] passwordInitial;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Generate(length, out passwordInitial), "Generating password failed.");

            byte[] passwordDifferent;
            Assert.IsTrue(Solomon.Common.Cryptography.Password.Generate(length, out passwordDifferent), "Generating password failed.");

            Assert.IsFalse(Solomon.Common.Cryptography.Password.SlowEquals(passwordInitial, passwordDifferent), "Slow equals should have failed.");
        }
    }
}
