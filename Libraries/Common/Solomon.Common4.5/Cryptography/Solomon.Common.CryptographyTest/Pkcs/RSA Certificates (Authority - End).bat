rem		Makecert:	https://msdn.microsoft.com/en-us/library/windows/desktop/aa386968.aspx
rem					https://msdn.microsoft.com/library/bfsktky3(v=vs.100).aspx
rem		Store name:	https://msdn.microsoft.com/en-us/library/system.security.cryptography.x509certificates.storename.aspx
rem		Providers:	https://msdn.microsoft.com/en-us/library/windows/desktop/bb931357(v=vs.85).aspx
rem		http://www.jayway.com/2014/09/03/creating-self-signed-certificates-with-makecert-exe-for-development/

rem		Certificate validity should be no more than 39 months.
rem		-m 39

rem		Should also tie the serial number to the client key.
rem		-# 1234567890

		cd C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\

rem		Private Key Password / Issuer Signature:	5DDD226A-8431-4D9B-8FEB-466F73A3	// Use only the 32 characters

		makecert ^
		-pe ^
		-r ^
		-n "CN=HSB Solomon Associates LLC Client Certificate Authority" ^
		-cy authority ^
		-a sha512 -len 4096 ^
		-sv "C:\Authority.pvk" "C:\Authority.cer"

rem		Private Key Password:						E3BD99F7-84B2-4E4E-ABF9-083D1C47
rem		Issuer Signature (Use Authority Password)

		makecert ^
		-pe ^
		-n "CN=Yanbu Aramco Sinopec Refining Company (355EUR),O=Yanbu,OU=355EUR,C=SAU" ^
		-b 12/15/2015 -e 01/31/2021 ^
		-cy end ^
		-a sha512 -len 4096 ^
		-sky exchange -eku 1.3.6.1.5.5.7.3.1 ^
		-iv "C:\Authority.pvk" -ic "C:\Authority.cer" ^
		-sp "Microsoft RSA SChannel Cryptographic Provider" ^
		-sy 12 ^
		-sv "C:\Confidentiality.pvk" "C:\Confidentiality.cer"

		pvk2pfx -pvk "C:\Confidentiality.pvk" -spc "C:\Confidentiality.cer" -pfx "C:\Confidentiality.pfx" -pi "E3BD99F7-84B2-4E4E-ABF9-083D1C47" -f
