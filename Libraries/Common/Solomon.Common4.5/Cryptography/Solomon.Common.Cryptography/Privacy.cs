﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Solomon.Common.Cryptography
{ 
    /// <summary>
    /// Question:	How do you secure the data from unauthorized parties?
    /// Objective:	Encrypt data to be shared.
    /// </summary>
    public static class Privacy
    {
        /// <summary>
        /// Objective:	Encrypt and decrypt a message to be sent from one party to another using a shared secret privacy key.
        /// Implies:	Two parties share the same secret privacy key to encrypt and decrypt the text.
        /// </summary>
        /// <remarks>
        /// sharedPrivacyKey SHOULD be asymmetrically encrypted; the key will be encrypted, shared, then decrypted.
        /// sharedPrivacyKey MUST shared 'out-of' band.
        /// Utilizes FIPS compliant AES algorithm. The shared secret key must be 16, 24, or 32 bytes (128, 192, or 256 bits).
        /// </remarks>
        public static class Symmetric
        {
            private const int AesKeySize = 256;
            private const int AesBlockSize = 128;
            private const int AesFeedbackSize = 128;

            private const CipherMode AesCipherMode = CipherMode.CBC;
            private const PaddingMode AesPaddingMode = PaddingMode.ISO10126;

            /// <summary>
            /// Encrypts a message using a shared secret privacy key.
            /// </summary>
            /// <param name="plainText"></param>
            /// <param name="sharedPrivacyKey">Must be 16, 24, or 32 bytes (128, 192, or 256 bits).</param>
            /// <param name="cipherText">Encrypted byte array preceded by a random IV (Initialization Vector).</param>
            /// <returns>Success or failure of the enryption; Zero byte array is created if enryption fails.</returns>
            public static bool Encrypt(byte[] plainText, byte[] sharedPrivacyKey, out byte[] cipherText)
            {
                bool encryptBytes = false;

                try
                {
                    using (AesManaged aes = new AesManaged())
                    {
                        aes.BlockSize = AesBlockSize;
                        aes.FeedbackSize = AesFeedbackSize;
                        aes.KeySize = AesKeySize;
                        aes.Mode = AesCipherMode;
                        aes.Padding = AesPaddingMode;

                        aes.Key = sharedPrivacyKey;

                        using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                        {
                            rng.GetBytes(aes.IV);
                        }

                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(plainText, 0, plainText.Length);
                                cs.Flush();
                                cs.Close();
                            }
                            cipherText = aes.IV.Concat(ms.ToArray()).ToArray();
                            encryptBytes = true;
                            ms.Flush();
                            ms.Close();
                            aes.Clear();
                        }
                    }
                }
                catch
                {
                    cipherText = new byte[0];
                    encryptBytes = false;
                }
                return encryptBytes;
            }

            /// <summary>
            /// Decrypts a message using a shared secret privacy key.
            /// </summary>
            /// <param name="cipherText">Encrypted byte array preceded by a random IV (Initialization Vector).</param>
            /// <param name="sharedPrivacyKey">Must be 16, 24, or 32 bytes (128, 192, or 256 bits).</param>
            /// <param name="plainText"></param>
            /// <returns>Success or failure of the decryption; Zero byte array is created if decryption fails.</returns>
            public static bool Decrypt(byte[] cipherText, byte[] sharedPrivacyKey, out byte[] plainText)
            {
                bool decryptBytes = false;

                try
                {
                    using (AesManaged aes = new AesManaged())
                    {
                        aes.BlockSize = AesBlockSize;
                        aes.FeedbackSize = AesFeedbackSize;
                        aes.KeySize = AesKeySize;
                        aes.Mode = AesCipherMode;
                        aes.Padding = AesPaddingMode;

                        aes.IV = Split.Left(cipherText, aes.IV.Length, out cipherText);
                        aes.Key = sharedPrivacyKey;

                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(cipherText, 0, cipherText.Length);
                                cs.Flush();
                                cs.Close();
                            }
                            plainText = ms.ToArray();
                            decryptBytes = true;
                            ms.Flush();
                            ms.Close();
                            aes.Clear();
                        }
                    }
                }
                catch
                {
                    plainText = new byte[0];
                    decryptBytes = false;
                }
                return decryptBytes;
            }
        }
        
        /// <summary>
        /// Objective:	Encrypt and decrypt a message to be sent from one party to another using public-private key pairs.
        /// Implies:	Encrypting with receiver's public key; decrypting with receiver's private key.
        /// </summary>
        /// <remarks>Utilizes FIPS compliant RSA algorithm. Plain byte array must be less than 471 bytes (3768 bits).</remarks>
        public static class Asymmetric
        {
            /// <summary>
            /// Encrypts a message using a public key (utilizing a FIPS compliant algorithm).
            /// If the encryption fails, a zero byte array is returned.
            /// </summary>
            /// <param name="plainText">Must be less than 215 bytes (1720 bits, 215 Characters).</param>
            /// <param name="publicCertificate"></param>
            /// <param name="cipherText"></param>
            /// <returns>Success or failure of the encryption; Zero byte array is created if encryption fails.</returns>
            /// <remarks>
            /// The amount of data that can be encrypted is based on the key length.
            ///		2048:	214 (Default for proper key creation)
            ///		4096	470
            /// </remarks>
            public static bool Encrypt(byte[] plainText, X509Certificate2 publicCertificate, out byte[] cipherText)
            {
                try
                {
                    using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)publicCertificate.PublicKey.Key)
                    {
                        cipherText = rsa.Encrypt(plainText, true);// RSAEncryptionPadding.OaepSHA1);
                        rsa.Clear();
                        return true;
                    }
                }
                catch
                {
                    cipherText = new byte[0];
                    return false;
                }
            }

            /// <summary>
            /// Decrypts a message using a private key (utilizing a FIPS compliant algorithm).
            /// If the encryption fails, a zero byte array is returned.
            /// </summary>
            /// <param name="cipherText"></param>
            /// <param name="privateCertificate"></param>
            /// <param name="plainText"></param>
            /// <returns>Success or failure of the decryption; Zero byte array is created if decryption fails.</returns>
            public static bool Decrypt(byte[] cipherText, X509Certificate2 privateCertificate, out byte[] plainText)
            {
                try
                {
                    if (privateCertificate.HasPrivateKey)
                    {
                        using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)privateCertificate.PrivateKey)
                        {
                            plainText = rsa.Decrypt(cipherText,true); //RSAEncryptionPadding.OaepSHA1);
                            rsa.Clear();
                            return true;
                        }
                    }
                    else
                    {
                        plainText = new byte[0];
                        return false;
                    }
                }
                catch
                {
                    plainText = new byte[0];
                    return false;
                }
            }
        }
        
    }

}
