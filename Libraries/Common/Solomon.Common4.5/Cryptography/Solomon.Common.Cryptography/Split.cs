﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Common.Cryptography
{    static class Split
    {
        /// <summary>
        /// Separates raw data to left and right parts.
        /// </summary>
        /// <param name="raw">[left bytes][right bytes]</param>
        /// <param name="leftLength">Length of the left byte array.</param>
        /// <param name="right"></param>
        /// <returns>First raw[leftLength] bytes</returns>
        internal static byte[] Left(byte[] raw, int leftLength, out byte[] right)
        {
            right = raw.Skip(leftLength).Take(raw.Length - leftLength).ToArray();
            return raw.Take(leftLength).ToArray();
        }
    }
}
